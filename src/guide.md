*** DEPENDENCIES ***
Finished: 9th Feb 2022
1. npm install @mui/material @emotion/react @emotion/styled
2. npm install font-awesome --save
3. npm i axios
4. npm i react-redux
5. npm i redux
6. npm i react-router-dom
7. npm i redux-thunk
8. npm i redux-logger
9. npm install json-server
10. npm install concurrently
11. Vs code snippets (rafce to generate component snippet) => ES7+ React/Redux/React-Native snippets
12. 
￼
*** STEPS ***
1. npx create-react-app@5.0.0 my-app
2. npm install @mui/material @emotion/react @emotion/styled
3. npm install @mui/styles
4. Add bootstrap cdn
5. Paste this in index.js => import "bootstrap-icons/font/bootstrap-icons.css";  and import 'font-awesome/css/font-awesome.min.css';
6. npm i bootstrap-icons
7. npm install font-awesome --save
8. npm i react-router-dom
9. Npm i redux react-redux
10. Npm install -g json-server … install globally
11. Npm install json-server … locally
12. Under package.json -> “scripts” -> add -> “server”: “json-server - -watch src/Database/db.json - -port 5000” 
13. Terminal: npm run server 
14. Npm install concurrently
15. Under package.json -> “scripts” -> add -> "dev": "concurrently \"npm start\" \"npm run server\" " 
16. Terminal: npm run dev
17. Npm install axios
18. Resource => https://www.youtube.com/watch?v=hXpYQqykORU ... code with Vishal 
19. Article (useState -> Forms) => https://dev.to/jleewebdev/using-the-usestate-hook-and-working-with-forms-in-react-js-m6b

*** REACT HOOKS ***
The useDispatch hook is used to dispatch an action while useSelector hook is used to get the state from the redux store
OR
useDispatch is used to modify values of a state while the useSelector hook is used to access values of the state.

useEffect is called whenever a component is mounted or when it’s updated.
1. Call useEffect whenever a state in your component gets changed. In the example below, useEffect will be called whenever either time or message states are changed. Hence the console log will be logged in both updates.
	const [time, setTime] = useState(“”);
	const [message, setMessage] = useState(“”);

	useEffect(() => {
		console.log(“Log Log Log”);
	});

2. Call useEffect only when the component is mounted and not when the states are updated. In the example below, useEffect will only be called the first time the component is mounted and not when any of the states change. After the log is output the first time during component mounting, it won’t happen again even if the states get updated.
	const [time, setTime] = useState(“”);
	const [message, setMessage] = useState(“”);

	useEffect(() => {
		console.log(“This will only log during mounting”);
	}, []);

3. Call useEffect only when one state changes and not when another state changes. In the example below, useEffect will only be called when the time state changes and not when the message state changes. This is because, the time variable is passed as a dependency in the array in useEffect hook.
	const [time, setTime] = useState(“”);
	const [message, setMessage] = useState(“”);

	useEffect(() => {
		console.log(“This will only log when the state of the time variable changes”);
	}, [time]);
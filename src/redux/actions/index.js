import axios from "axios";
import { ActionTypes } from "./actionTypes";

export const getUsersAction = (users) => ({
    type: ActionTypes.GET_USERS,
    payload: users
});

export const deleteUserAction = () => ({
    type: ActionTypes.DELETE_USER
});

export const addUserAction = () => ({
    type: ActionTypes.ADD_USER
});

export const getUserAction = (user) => ({
    type: ActionTypes.GET_SINGLE_USER,
    payload: user
});

export const updateUserAction = () => ({
    type: ActionTypes.UPDATE_USER
});

export const getAllUsers = () => {
    return async function (dispatch) {
        await axios
            .get(`${process.env.REACT_APP_API}`)
            .then((response) => {
                console.log("Response", response);
                dispatch(getUsersAction(response.data));
            })
            .catch((error) => console.log("Error", error));
    }
}

export const deleteUser = (id) => {
    return async function (dispatch) {
        await axios
            .delete(`${process.env.REACT_APP_API}/${id}`)
            .then((response) => {
                console.log("Response", response);
                dispatch(deleteUserAction());
                dispatch(getAllUsers());
            })
            .catch((error) => console.log("Error", error));
    }
}

export const addUser = (user) => {
    return async function (dispatch) {
        await axios
            .post(`${process.env.REACT_APP_API}`, user)
            .then((response) => {
                console.log("Response", response);
                dispatch(addUserAction());
                dispatch(getAllUsers());
            })
            .catch((error) => console.log("Error", error));
    }
} 

export const getSingleUser = (id) => {
    return async function (dispatch) {
        await axios
            .get(`${process.env.REACT_APP_API}/${id}`)
            .then((response) => {
                console.log("Response", response);
                dispatch(getUserAction(response.data));
            })
            .catch((error) => console.log("Error", error));
    }
}

export const updateUser = (id, user) => {
    return async function (dispatch) {
        await axios
            .put(`${process.env.REACT_APP_API}/${id}`, user)
            .then((response) => {
                console.log("Response", response);
                dispatch(updateUserAction());
                dispatch(getAllUsers());
            })
            .catch((error) => console.log("Error", error));
    }
}


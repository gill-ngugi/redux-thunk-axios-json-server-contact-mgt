import { ActionTypes } from "../actions/actionTypes";

const initialState = {
    users: [],
    user: {},
    loading: true
}

const usersReducer = (state=initialState, action) => {
    switch(action.type){
        case ActionTypes.GET_USERS:  
            return {
                ...state,
                users: action.payload,
                loading: false
            }

        case ActionTypes.DELETE_USER:
            return {
                ...state,
                loading: false
            }

        case ActionTypes.ADD_USER:
            return {
                ...state,
                loading: false
            }

        case ActionTypes.GET_SINGLE_USER:
            return {
                ...state,
                user: action.payload,
                loading: false
            }

        case ActionTypes.UPDATE_USER:
            return {
                ...state,
                loading: false
            }

        default: 
            return state;
    }
}

export default usersReducer;
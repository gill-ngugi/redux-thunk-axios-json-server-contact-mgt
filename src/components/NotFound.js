import { NavLink } from "react-router-dom";
import user from "../images/user.png";

const NotFound = () => {
    const all = {
        textAlign: "center"
    }

    const imageA = {
        width: "50%",
        height: "40%"
    }

    const link = {
        fontSize: "25px",
        textDecoration: "none"
    }

    return (
        <div style={all}>
            <h1>Page Not Found</h1>
            <NavLink to="/" style={link}>
                <h4>Click to go to Homepage</h4>
            </NavLink>
            <img src={user} alt="imageA" style={imageA} />
           
        </div>
    );
}

export default NotFound;
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getAllUsers, deleteUser } from "../redux/actions";
import { useNavigate } from "react-router";

const Home = () => {
    const styles = {
        mainDiv: {
            paddingTop: "1%",
            textAlign: "center"
        },
        addUserBtn: {
            marginTop: "0.5%",
            fontWeight: "bolder"
        },
        icons: {
            marginRight: "10px"
        },
        table: {
            width: "70%",
            margin: "auto",
            marginTop: "1.5%"
        },
        thead: {
            backgroundColor: "#2A9DF4"
        },
        tr: {
            lineHeight: "50px",
            minHeight: "50px",
            height: "50px"
        },
        btn: {
            width: "100px",
            marginRight: "15px"
        }
    }

    const { users } = useSelector(state => state.users);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleDelete = (id) => {
        if(window.confirm("Are you sure you want to delete this user?")) {
            dispatch(deleteUser(id));
        }
    }

    useEffect(() => {
        dispatch(getAllUsers());
    }, []);

    return (
        <div style={styles.mainDiv}>
            <h2>CONTACT MANAGEMENT</h2>
            <NavLink type="button" className="btn btn-primary" style={styles.addUserBtn} to="/add">
                <i className="fa fa-address-card-o" aria-hidden="true" style={styles.icons} />
                Add A Contact
            </NavLink>
            <table className="table table-striped" style={styles.table}>
                <thead style={styles.thead}>
                    <tr style={styles.tr}>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Contact</th>
                        <th scope="col">Address</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map((user) => (
                        <tr style={styles.tr} key={user.id}>
                            <th scope="row">{user.name}</th>
                            <td>{user.email}</td>
                            <td>{user.address}</td>
                            <td>{user.contact}</td>
                            <td>
                                <button type="button" className="btn btn-warning" style={styles.btn}
                                    onClick={() => navigate(`/edit/${user.id}`)} >
                                    <i className="fa fa-pencil" aria-hidden="true" style={styles.icons}></i>
                                    Edit
                                </button>
                                <button type="button" className="btn btn-danger" style={styles.btn} 
                                    onClick={() => handleDelete(user.id)} >
                                    <i className="fa fa-trash-o" aria-hidden="true" style={styles.icons}></i>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Home;

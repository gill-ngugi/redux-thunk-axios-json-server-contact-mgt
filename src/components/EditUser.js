import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { updateUser } from '../redux/actions';
import { useNavigate } from 'react-router';
import { useParams } from 'react-router';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { getSingleUser } from '../redux/actions';

const EditUser = () => {
    const styles = {
        container: {
            width: "60%",
            margin: "auto",
            textAlign: "center",
            alignContent: "center",
            paddingTop: "20px"
        },
        textField: {
            marginBottom: "15px"
        },
        span: {
            width: "10%"
        },
        buttons: {
            width: "100px",
            marginRight: "30px"
        }
    }

    const [singleUser, setSingleUser] = useState({
        name: "",
        email: "",
        address: "",
        contact: ""
    });

    const { name, email, address, contact } = singleUser;

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { id } = useParams();
    const { user } = useSelector((state) => state.users);

    const clearForm = () => {
        setSingleUser({
            name: "",
            email: "",
            address: "",
            contact: ""
        })
    }

    const [error, setError] = useState("");

    //use this as onChange={handleInputChange}
    const handleInputChange = (e) => {
        let { name, value } = e.target;
        setSingleUser({ ...singleUser, [name]: value });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!name || !email || !address || !contact) {
            setError("All fields are required!");
        }
        else {
            dispatch(updateUser(id, singleUser));
            setError("");
            clearForm();
            navigate("/");
        }
    }

    useEffect(() => {
        dispatch(getSingleUser(id));
    }, []); 

    useEffect(() => {
        if(user) {
            setSingleUser({ ...user });
        }
    }, [user]);

    return (
        <div style={styles.container}>
            <h4>Edit Contact</h4>
            <NavLink className="btn btn-primary" type="button" value="Back To Homepage"
                style={{ width: "20%", marginBottom: "2%" }}
                to="/">
                Back To Homepage
            </NavLink>
            <form onSubmit={handleSubmit}>
                {error && <h3 style={{ color: "red" }}>{error}</h3>}
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>Name</span>
                    <input type="text" className="form-control" aria-label="Name" name="name"
                        value={name || ""}
                        onChange={(e) => setSingleUser({ ...singleUser, name: e.target.value })} />
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>Email</span>
                    <input type="email" className="form-control" aria-label="Email" name="email"
                        value={email || ""}
                        onChange={(e) => setSingleUser({ ...singleUser, email: e.target.value })} />
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>Address</span>
                    <input type="text" className="form-control" aria-label="Address" name="address"
                        value={address || ""}
                        onChange={(e) => setSingleUser({ ...singleUser, address: e.target.value })} />
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1" style={styles.span}>Contact</span>
                    <input type="number" className="form-control" aria-label="Contact" name="contact"
                        value={contact || ""}
                        onChange={(e) => setSingleUser({ ...singleUser, contact: e.target.value })} />
                </div>
                <div>
                    <input className="btn btn-primary" type="reset" value="Clear" style={styles.buttons}
                        onClick={() => clearForm()} />
                    <input className="btn btn-primary" type="submit" value="Update" style={styles.buttons} />
                </div>
            </form>
        </div>
    );
}

export default EditUser;
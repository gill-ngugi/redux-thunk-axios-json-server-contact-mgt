import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './components/Home';
import AddUser from './components/AddUser';
import EditUser from './components/EditUser';
import NotFound from './components/NotFound';

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="*" element={ <NotFound /> } />
          <Route path="/" exact element={ <Home /> } />
          <Route path="/add" exaxt element= { <AddUser /> } />
          <Route path="/edit/:id" exact element={ <EditUser /> } />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
